import {userStub} from "../test/stubs/user.stub";

export const UserService = jest.fn().mockReturnValue({
    getUserById: jest.fn().mockReturnValue(userStub())
})
