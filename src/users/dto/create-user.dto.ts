import { ApiProperty } from '@nestjs/swagger';

export class CreateUserDto {
  @ApiProperty({ example: 'mail@gmail.com', description: 'Email of user' })
  readonly email: string;
  @ApiProperty({ example: 'qwerty', description: 'Password of user' })
  readonly password: string;
}
