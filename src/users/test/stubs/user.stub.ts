export const userStub = () => {
    return {
        userId: '123',
        email: 'test@ex.com',
        password: 'pass',
        banned: false,
        bannedReason: true
    }
}
