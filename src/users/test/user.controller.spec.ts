import {Test} from "@nestjs/testing";
import {UsersController} from "../users.controller";
import {UsersService} from "../users.service";
import {userStub} from "./stubs/user.stub";

jest.mock('../users.service');

describe('UserController', () => {
    let userController: UsersController
    let userService: UsersService

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            imports: [],
            controllers: [UsersController],
            providers: [UsersService],
        }).compile();

        userController = moduleRef.get<UsersController>(UsersController);
        userService = moduleRef.get<UsersService>(UsersService);
        jest.clearAllMocks()

    })

    describe('getUser', () => {
        describe('when getUser is called', () => {
            let user

            beforeEach(async () => {
                user = await userController.getUserById(userStub().userId)
            })

            test('then it should call userService', () => {
                expect(userService.getUserById).toBeCalledWith(userStub().userId)
            })
        })
    })
})
