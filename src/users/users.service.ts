import { Injectable } from '@nestjs/common';
import { User } from './users.model';
import { InjectModel } from '@nestjs/sequelize';
import { CreateUserDto } from './dto/create-user.dto';

@Injectable()
export class UsersService {
  constructor(@InjectModel(User) private userRepo: typeof User) {}

  async createUser(dto: CreateUserDto) {
    const user = await this.userRepo.create(dto);
    return user;
  }

  async getAllUsers() {
    const users = await this.userRepo.findAll();
    return users;
  }

  async getUserById(id: number) {
    const userById = await this.userRepo.findOne({ where: { id } });
    return userById;
  }

  async deleteUser(id: number) {
    await this.userRepo.destroy({ where: { id } });
    return 'User was deleted';
  }

  async updateUser(id: number, dto: CreateUserDto) {
    await this.userRepo.update(dto, { where: { id } });
    return 'User was updated';
  }
}
